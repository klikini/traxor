(function () {
	const recordsURL = document.getElementById("RecordsURL").href;
	const mapSettingsForm = document.getElementById("MapSettings");

	const map = L.map("Map", {
		center: [0, 0],
		zoom: 2,
		minZoom: 2,
		layers: [
			L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
				maxZoom: 19,
				attribution: '© <a href="https://www.openstreetmap.org/">OpenStreetMap</a> | <a href="https://www.openstreetmap.org/fixthemap">Report a problem</a>',
			})
		],
	});

	async function getRecords() {
		const query = new URLSearchParams(Object.fromEntries(new FormData(mapSettingsForm)));
		const records = await fetch(recordsURL + "?" + query).then(r => r.json());
		console.log(records);
	}

	mapSettingsForm.addEventListener("submit", function (event) {
		event.preventDefault();
		getRecords();
	});
})();

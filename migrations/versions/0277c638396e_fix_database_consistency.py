"""fix database consistency

Revision ID: 0277c638396e
Revises: b45e3d7020e6
Create Date: 2023-09-02 22:01:10.599598

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0277c638396e'
down_revision = 'b45e3d7020e6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('location_reports', schema=None) as batch_op:
        batch_op.alter_column('place_id',
               existing_type=sa.INTEGER(),
               type_=sa.BigInteger(),
               existing_nullable=True)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('location_reports', schema=None) as batch_op:
        batch_op.alter_column('place_id',
               existing_type=sa.BigInteger(),
               type_=sa.INTEGER(),
               existing_nullable=True)

    # ### end Alembic commands ###

from gunicorn.arbiter import Arbiter

bind = "127.0.0.1:8000"
wsgi_app = "app:app"
workers = 2
worker_class = "gthread"
preload_app = True
timeout = 300


def on_starting(server: Arbiter):
    pass

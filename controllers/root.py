from flask import Blueprint, redirect, url_for, render_template
from flask_login import login_required

root_blueprint = Blueprint("root", __name__)


@root_blueprint.get("/")
@login_required
def index():
    return render_template("home.html")


@root_blueprint.get("/map")
@login_required
def map_view():
    return render_template("map.html")

from logging import getLogger

from flask import (
    Blueprint,
    request,
    flash,
    redirect,
    render_template,
    url_for,
    make_response,
    send_from_directory,
    current_app,
    abort,
)
from flask_login import current_user, login_required
from werkzeug.security import generate_password_hash

from models import User
from models.database import db

users_blueprint = Blueprint("users", __name__, url_prefix="/users")

_logger = getLogger(__name__)


@users_blueprint.get("/")
@login_required
def user_list():
    return render_template("users/list.html", users=User.query.order_by(User.username))


@users_blueprint.post("/")
def manage_user():
    match request.form.get("action"):
        case "add":
            username = request.form.get("username").strip()
            password = request.form.get("password")
            db.session.add(User(username=username, password=password))
            db.session.commit()
            flash("User created successfully")
        case "delete":
            user_id = int(request.form.get("user_id"))

            if user_id == current_user.id:
                abort(403)

            User.query.filter(User.id == user_id).delete()
            db.session.commit()
            flash("User deleted successfully")

    return redirect(url_for(".user_list"))


@users_blueprint.get("/<username>/avatar")
@login_required
def avatar(username: str):
    if user := User.query.filter(
        User.username == username,
        User.avatar.is_not(None),
        User.avatar_type.is_not(None),
    ).one_or_none():
        response = make_response(user.avatar)
        response.content_type = user.avatar_type
        return response

    return send_from_directory(current_app.static_folder, "img/account.svg")


@users_blueprint.get("/me")
@login_required
def profile():
    return render_template("users/profile.html")


@users_blueprint.post("/me")
@login_required
def save_profile():
    user: User = current_user

    if username := request.form.get("username"):
        user.username = username

    if password := request.form.get("password"):
        user.password = generate_password_hash(password)

    if new_avatar := request.files.get("avatar"):
        user.avatar = new_avatar.stream.read()
        user.avatar_type = new_avatar.content_type
    elif request.form.get("action") == "clear_avatar":
        user.avatar = None

    if ha_instance := request.form.get("ha_instance"):
        user.ha_instance = ha_instance

    if ha_token := request.form.get("ha_token"):
        user.ha_token = ha_token

    if ha_entity := request.form.get("ha_entity"):
        user.ha_entity = ha_entity

    db.session.commit()
    flash("Changes saved successfully.")
    return redirect(url_for(".profile"))

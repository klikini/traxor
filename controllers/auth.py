from logging import getLogger

from flask import Blueprint, request, flash, redirect, render_template, url_for, session
from flask_login import login_user, logout_user
from werkzeug.security import check_password_hash

from models import User

auth_blueprint = Blueprint("auth", __name__, url_prefix="/auth")

_logger = getLogger(__name__)


@auth_blueprint.get("/login")
def login_form():
    return render_template("auth/login.html")


@auth_blueprint.post("/login")
def login_submit():
    if not (username := request.form.get("username")):
        flash("Username is required", "error")
        return render_template("auth/login.html")

    if not (
        user := User.query.filter(User.username == username).one_or_none()
    ) or not check_password_hash(user.password, request.form.get("password")):
        flash("Invalid username or password", "error")
        return render_template("auth/login.html", username=username)

    _logger.info(
        f"Successful login from {request.remote_addr} by user '{user.username}'"
    )
    login_user(user, remember=True)
    return redirect(session.get("next") or url_for("root.index"))


@auth_blueprint.route("/logout")
def logout():
    logout_user()
    return redirect(url_for(".login_form"))

from datetime import datetime
from logging import getLogger

from flask import Blueprint, request, jsonify
from flask_login import current_user, login_required

from models import LocationReport
from models.database import db

owntracks_blueprint = Blueprint("owntracks", __name__, url_prefix="/ot")

_logger = getLogger(__name__)


@owntracks_blueprint.post("/")
@login_required
def report():
    location = LocationReport(
        user_id=current_user.id,
        timestamp=datetime.fromtimestamp(request.json.get("tst")),
        latitude=request.json.get("lat"),
        longitude=request.json.get("lon"),
        lat_lng_accuracy=request.json.get("acc"),
        altitude=request.json.get("alt"),
        alt_accuracy=request.json.get("vac"),
        source="OwnTracks",
    )

    db.session.add(location)
    db.session.commit()
    _logger.info(f"User '{current_user.username}' reported location via OwnTracks")
    return jsonify([])

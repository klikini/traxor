from flask import Flask

from .auth import auth_blueprint
from .owntracks import owntracks_blueprint
from .places import places_blueprint
from .records import records_blueprint
from .root import root_blueprint
from .users import users_blueprint


def register_blueprints(app: Flask):
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(owntracks_blueprint)
    app.register_blueprint(places_blueprint)
    app.register_blueprint(records_blueprint)
    app.register_blueprint(root_blueprint)
    app.register_blueprint(users_blueprint)

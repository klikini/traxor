import json
import tarfile
from datetime import datetime
from logging import getLogger
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import Request, urlopen

from dateutil.parser import parse as parse_dt
from flask import Blueprint, render_template, jsonify, request, url_for, redirect, flash
from flask_login import current_user, login_required

from models import LocationReport, Place, User
from models.database import db

records_blueprint = Blueprint("records", __name__, url_prefix="/records")

_logger = getLogger(__name__)


@records_blueprint.get("/")
@login_required
def get_records():
    """
    :returns: a JSON array of location records
    """
    query = LocationReport.query.filter(LocationReport.user_id == current_user.id)

    return jsonify([dict(r) for r in query])


@records_blueprint.get("/import")
@login_required
def import_view():
    return render_template("records/import.html")


@records_blueprint.post("/import")
@login_required
def do_import():
    if file := request.files.get("google_location_history"):
        do_google_import(file)
    elif request.form.get("hass") == "import":
        do_hass_import()

    return redirect(url_for(".import_view"))


def do_hass_import():
    user: User = current_user

    if not user.ha_instance:
        flash("Home Assistant instance URL must be set in profile first", "error")
        return

    if not user.ha_token:
        flash("Home Assistant access token must be set in profile first", "error")
        return

    if not user.ha_entity:
        flash("Home Assistant entity ID must be set in profile first", "error")
        return

    if start := request.form.get("start"):
        start = parse_dt(start)
    else:
        if last_report := user.location_reports.order_by(
            LocationReport.timestamp.desc()
        ).first():
            start = last_report.timestamp
        else:
            flash(
                "A start date is required because you have no location reports yet",
                "error",
            )
            return

    if end := request.form.get("end"):
        end = parse_dt(end)
    else:
        end = datetime.now()

    hass_request = Request(
        user.ha_instance.rstrip("/")
        + f"/api/history/period/{start.strftime('%Y-%m-%dT%H:%M:%S%z')}?"
        + urlencode(
            {
                "filter_entity_id": user.ha_entity,
                "end_time": end.strftime("%Y-%m-%dT%H:%M:%S%z"),
            },
        ),
        headers={
            "Authorization": f"Bearer {user.ha_token}",
        },
    )

    _logger.info(f"Requesting from Home Assistant: {hass_request.full_url}")

    try:
        with urlopen(hass_request) as hass_response:
            history = json.load(hass_response)[0]
    except HTTPError as e:
        flash(f"Error from Home Assistant: {e}", "error")
        return

    new_reports = []

    for entry in history:
        timestamp = parse_dt(entry["last_changed"])
        source_entity = (
            entry["attributes"].get("source")
            or entry["attributes"].get("friendly_name")
            or user.ha_entity
        )

        new_reports.append(
            LocationReport(
                user_id=user.id,
                timestamp=timestamp,
                latitude=entry["attributes"]["latitude"],
                longitude=entry["attributes"]["longitude"],
                lat_lng_accuracy=entry["attributes"].get("gps_accuracy"),
                altitude=entry["attributes"].get("altitude"),
                alt_accuracy=entry["attributes"].get("vertical_accuracy"),
                source=f"Home Assistant ({source_entity})",
            )
        )

    db.session.bulk_save_objects(new_reports)
    db.session.commit()
    flash(f"Imported {len(new_reports):,} record(s) from Home Assistant")


def do_google_import(file):
    added_records = 0
    added_places = 0

    with tarfile.open(fileobj=file.stream, mode="r|gz") as archive:
        for member in archive:
            if (
                member.type not in tarfile.REGULAR_TYPES
                or not member.name.startswith(
                    "Takeout/Location History/Semantic Location History/"
                )
                or not member.name.endswith(".json")
            ):
                continue

            _logger.info(f"Importing '{member.name}'")

            month_file = archive.extractfile(member)
            month_object = json.load(month_file)

            for obj in month_object["timelineObjects"]:
                # obj contains one key, either "placeVisit" or "activitySegment"
                if place_visit := obj.get("placeVisit"):
                    ar, ap = add_google_visit(place_visit)
                    added_records += ar
                    added_places += ap

    flash(
        f"Imported {added_records:,} location record(s) and {added_places:,} place(s)"
    )
    db.session.commit()


def add_google_visit(place_visit: dict) -> tuple[int, int]:
    added_records = 0
    added_places = 0

    if location := place_visit.get("location"):
        try:
            latitude = (location.get("latitudeE7") or location["latE7"]) * (10**-7)
            longitude = (location.get("longitudeE7") or location["lngE7"]) * (10**-7)
            lat_lng_accuracy = location.get("accuracyMeters") or 0
        except KeyError:
            return added_records, added_places  # no location coordinates

        name = location.get("name")
        address = location.get("address")
    else:
        latitude = place_visit["latE7"] * (10**-7)
        longitude = place_visit["lngE7"] * (10**-7)
        lat_lng_accuracy = place_visit.get("accuracyMeters") or 0
        name = None
        address = None

    if duration := place_visit.get("duration"):
        start = parse_dt(duration.get("start") or duration["startTimestamp"])
        end = parse_dt(duration.get("end") or duration["endTimestamp"])
    elif timestamp := place_visit.get("timestamp"):
        start = parse_dt(timestamp)
        end = None
    else:
        return added_records, added_places  # no time information

    if simplified_raw_path := place_visit.get("simplifiedRawPath"):
        for path_entry in simplified_raw_path["points"]:
            ar, ap = add_google_visit(path_entry)
            added_records += ar
            added_places += ap

    if child_visits := place_visit.get("childVisits"):
        for child_visit in child_visits:
            ar, ap = add_google_visit(child_visit)
            added_records += ar
            added_places += ap

    if name or address:
        coordinates = (longitude, latitude)
        name = name or address
        place = Place.find(name, address, coordinates)

        if not place:
            place = Place(name=name, address=address, user_id=current_user.id)
            db.session.add(place)
            added_places += 1

        place.add_sample(horizontal=coordinates)
        db.session.flush()
        place_id = place.id
    else:
        place_id = None

    report = LocationReport(
        user_id=current_user.id,
        timestamp=start,
        end_timestamp=end,
        latitude=latitude,
        longitude=longitude,
        lat_lng_accuracy=lat_lng_accuracy,
        place_id=place_id,
        source="Google Location History",
    )

    db.session.add(report)
    added_records += 1
    return added_records, added_places

from logging import getLogger

from flask import Blueprint, render_template, abort, redirect, url_for, request, flash
from flask_login import current_user, login_required

from models import Place, LocationReport
from models.database import db

places_blueprint = Blueprint("places", __name__, url_prefix="/places")

_logger = getLogger(__name__)


@places_blueprint.get("/")
@login_required
def place_list():
    my_places = Place.query.filter(Place.user_id == current_user.id).order_by(Place.name)
    shared_places = Place.query.filter(Place.shared).order_by(Place.name)

    return render_template("places/list.html", my_places=my_places, shared_places=shared_places)


@places_blueprint.get("/<int:place_id>")
@login_required
def details(place_id: int):
    place = Place.query.get_or_404(place_id)

    if place.user_id != current_user.id and not place.shared:
        abort(403)

    return render_template("places/details.html", place=place)


@places_blueprint.post("/<int:place_id>")
@login_required
def edit(place_id: int):
    place = Place.query.get_or_404(place_id)

    if place.user_id != current_user.id and not place.shared:
        abort(403)

    if name := request.form.get("name"):
        place.name = name

    if address := request.form.get("address"):
        place.address = address

    place.shared = bool(request.form.get("shared"))

    db.session.commit()
    flash("Place updated successfully")

    return redirect(url_for(".details", place_id=place_id))


@places_blueprint.get("/<int:place_id>/delete")
@login_required
def confirm_delete(place_id: int):
    place = Place.query.get_or_404(place_id)

    if place.user_id != current_user.id and not place.shared:
        abort(403)

    return render_template(
        "places/delete.html",
        place=place,
        my_places=Place.query.filter(Place.id != place.id, Place.user_id == current_user.id).order_by(Place.name),
        shared_places=Place.query.filter(Place.id != place.id, Place.shared).order_by(Place.name),
    )


@places_blueprint.post("/<int:place_id>/delete")
@login_required
def delete(place_id: int):
    place = Place.query.get_or_404(place_id)

    if place.user_id != current_user.id and not place.shared:
        abort(403)

    if merge := request.form.get("merge", type=int):
        new_place = Place.query.get_or_404(merge)
        LocationReport.query.filter(LocationReport.place_id == place.id).update({"place_id": new_place.id})
        db.session.flush()

    db.session.delete(place)
    db.session.commit()
    flash("Place deleted successfully")
    return redirect(url_for(".place_list"))

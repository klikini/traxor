import logging
from os import getenv
from secrets import token_bytes

from flask import Flask, Request
from flask_login import LoginManager, current_user
from flask_migrate import Migrate
from werkzeug.security import check_password_hash

from controllers import register_blueprints
from models import User
from models.database import db

app = Flask(__name__)

logging.basicConfig(level=logging.DEBUG if app.debug else logging.INFO)

# region database

app.config["SQLALCHEMY_DATABASE_URI"] = getenv("DB_URI")
app.config["SECRET_KEY"] = getenv("SECRET_KEY") or token_bytes()

db.init_app(app)

migrate = Migrate()
migrate.init_app(app, db)

# endregion database

# region login

app.config["USE_SESSION_FOR_NEXT"] = True

login = LoginManager()
login.init_app(app)
login.login_view = "auth.login_form"


@login.user_loader
def user_loader(user_id: str) -> User | None:
    user_id = int(user_id)
    return User.query.filter(User.id == user_id).one_or_none()


@login.request_loader
def request_loader(request: Request) -> User | None:
    if not (auth := request.authorization) or not (
        basic_username := auth.get("username")
    ):
        return

    if not (user := User.query.filter(User.username == basic_username).one_or_none()):
        return

    if check_password_hash(user.password, request.authorization.get("password")):
        return user


# endregion login


@app.context_processor
def inject_context():
    return {
        "current_user": current_user,
    }


register_blueprints(app)

if __name__ == "__main__":
    app.run()

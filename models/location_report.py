from datetime import datetime
from typing import Optional

from sqlalchemy import BigInteger, ForeignKey
from sqlalchemy.orm import relationship, mapped_column, Mapped

from models.database import db


class LocationReport(db.Model):
    __tablename__ = "location_reports"

    # region columns

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id", ondelete="CASCADE"), index=True)

    timestamp: Mapped[datetime]

    end_timestamp: Mapped[Optional[datetime]]

    latitude: Mapped[float]

    longitude: Mapped[float]

    lat_lng_accuracy: Mapped[float]
    """Horizontal accuracy of the ``latitude`` and ``longitude`` fields."""

    altitude: Mapped[Optional[float]]

    alt_accuracy: Mapped[Optional[float]]
    """Vertical accuracy of the ``altitude`` field."""

    source: Mapped[Optional[str]]

    place_id: Mapped[Optional[int]] = mapped_column(ForeignKey("places.id", ondelete="SET NULL"), index=True)

    # endregion columns

    # region relationships

    user = relationship("User", back_populates="location_reports")

    place = relationship("Place", back_populates="location_reports")

    # endregion relationships

    def as_dict(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "timestamp": self.timestamp.timestamp(),
            "latitude": self.latitude,
            "longitude": self.longitude,
            "lat_lng_accuracy": self.lat_lng_accuracy,
            "altitude": self.altitude,
            "alt_accuracy": self.alt_accuracy,
            "source": self.source,
        }

from citext import CIText as _CIText
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class CIText(_CIText):
    """
    https://github.com/mahmoudimus/sqlalchemy-citext/issues/25
    https://github.com/mahmoudimus/sqlalchemy-citext/pull/28
    """

    cache_ok = True

from typing import Optional

from flask_login import current_user
from sqlalchemy import Column, BigInteger, func, UniqueConstraint, ForeignKey, or_
from sqlalchemy.orm import relationship, Query, mapped_column, Mapped

from models.database import db, CIText
from models.location_report import LocationReport


class Place(db.Model):
    __tablename__ = "places"

    __table_args__ = (UniqueConstraint("name", "address"),)

    # region columns

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id", ondelete="CASCADE"))

    shared: Mapped[bool] = mapped_column(default=False)
    """Whether other users can see this place."""

    name: Mapped[str]

    latitude: Mapped[float]

    longitude: Mapped[float]

    lat_lng_samples: Mapped[int] = mapped_column(default=0)
    """The number of pairs of latitude and longitude values that have been averaged."""

    altitude: Mapped[Optional[float]]

    alt_samples: Mapped[int] = mapped_column(default=0)
    """The number of altitude values that have been averaged."""

    address: Mapped[Optional[str]] = mapped_column(CIText)

    # endregion columns

    # region relationships

    user = relationship("User", back_populates="places")

    location_reports = relationship("LocationReport", back_populates="place", lazy="dynamic")

    # endregion relationships

    # region properties

    @property
    def my_visits(self) -> Query:
        return self.location_reports.filter(LocationReport.user_id == current_user.id).order_by(
            LocationReport.timestamp.desc()
        )

    # endregion properties

    # region instance methods

    def as_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "altitude": self.altitude,
            "address": self.address,
        }

    def add_sample(self, horizontal: tuple[float, float] = None, vertical: float = None):
        """
        :param horizontal: (longitude, latitude)
        :param vertical: altitude
        """
        if horizontal:
            if self.longitude and self.latitude:
                self.lat_lng_samples += 1
                self.longitude += (horizontal[0] - self.longitude) / self.lat_lng_samples
                self.latitude += (horizontal[1] - self.latitude) / self.lat_lng_samples
            else:
                self.lat_lng_samples = 1
                self.longitude, self.latitude = horizontal

        if vertical:
            if self.altitude:
                self.alt_samples += 1
                self.altitude += (vertical - self.altitude) / self.alt_samples
            else:
                self.alt_samples = 1
                self.altitude = vertical

    # endregion instance methods

    @staticmethod
    def find(name: str, address: str, location: tuple[float, float]):
        """
        :param name:
        :param address:
        :param location: (longitude, latitude)
        """
        # Check for duplicates that would violate the unique constraint
        if exact := Place.query.filter(Place.name == name, Place.address == address).first():
            return exact

        lng, lat = location
        derived_cols = []
        filter_by = []
        sort_by = []

        if address:
            derived_cols.append(func.levenshtein(Place.address, address).label("address_dist"))
            filter_by.append(Column("address_dist") < len(address) // 2)
            sort_by.append(Column("address_dist"))

        if name:
            derived_cols.append(func.levenshtein(Place.name, name).label("name_dist"))
            filter_by.append(Column("name_dist") < len(name) // 2)
            sort_by.append(Column("name_dist"))

        # Calculate the distances in a derived table so we can filter and sort by them
        derived = db.session.query(
            Place.id.label("place_id"),
            func.sqrt(func.pow(Place.longitude - lng, 2) + func.pow(Place.latitude - lat, 2)).label("dist"),
            *derived_cols,
        ).subquery()

        return (
            Place.query.join(derived, derived.c.place_id == Place.id)
            .filter(or_(Place.user_id == current_user.id, Place.shared), Column("dist") < 0.001, *filter_by)
            .order_by(Column("dist"), func.least(*sort_by).desc())
            .group_by(Place.id, Column("dist"), *sort_by)
        ).first()

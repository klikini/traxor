from typing import Optional

from flask_login import UserMixin
from sqlalchemy.orm import relationship, mapped_column, Mapped

from models.database import db, CIText


class User(db.Model, UserMixin):
    __tablename__ = "users"

    # region columns

    id: Mapped[int] = mapped_column(primary_key=True)

    username: Mapped[str] = mapped_column(CIText, unique=True)

    password: Mapped[str]
    """Hashed password."""

    ha_instance: Mapped[Optional[str]]
    """Home Assistant instance URL."""

    ha_token: Mapped[Optional[str]]
    """Home Assistant long-lived access token."""

    ha_entity: Mapped[Optional[str]]
    """Home Assistant entity ID."""

    avatar: Mapped[Optional[bytes]]
    """Avatar image."""

    avatar_type: Mapped[Optional[str]]
    """Avatar image MIME type."""

    # endregion columns

    # region relationships

    location_reports = relationship(
        "LocationReport", back_populates="user", uselist=True, lazy="dynamic"
    )

    places = relationship("Place", back_populates="user", uselist=True, lazy="dynamic")

    # endregion relationships

    def __init__(self, *args, **kwargs):
        db.Model.__init__(self, *args, **kwargs)
